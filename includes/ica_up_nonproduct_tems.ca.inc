<?php
/**
 * @file
 * Contains conditional action functions for ica uberpos mutatetotal.
 */
 
//create an entity for product amounts
function ica_up_nonproduct_items_ca_entity() {
  $entities['ica_product_amount'] = array(
    '#title' => t('Mutation amount for products'),
    '#type' => 'object',
  );
  return $entities;
}

//Trigger declaration 
function ica_up_nonproduct_items_ca_trigger() {
    
  $triggers['ica_uberpos_mutateamount'] = array(
    '#title' => t('Product amount mutated'),
    '#category' => t('ICA UberPOS'),
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order'),
      ),
      'product' => array(
        '#entity' => 'uc_order_product',
        '#title' => t('Product that was added'),
      ),
      'amount' => array(
        '#entity' => 'ica_product_amount',
        '#title' => t('Amount of products'),
      ),
    ),
  );
  
  return $triggers;
}

/**
 * Implementation of hook_action().
 */
//Action declaration
function ica_up_nonproduct_items_ca_action() {

  $actions['ica_up_nonproduct_items_action_mutate_total'] = array(
    '#title' => t('Mutate stock of a product from a specific store and adjusts total stock.'),
    '#callback' => 'ica_up_nonproduct_items_action_mutate_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
      'amount' => array('#entity' => 'ica_product_amount', '#title' => t('Product amount')),
    ),
    '#category' => t('Stock'),
  );
  $actions['ica_up_nonproduct_items_action_increment_refunded_stock'] = array(
    '#title' => t('Increment stock of a refunded product from a specific store and adjusts total stock.'),
    '#callback' => 'ica_up_nonproduct_items_action_increment_refunded_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
    ),
    '#category' => t('Stock'),
  );
  $actions['ica_up_nonproduct_items_decrement_stock'] = array(
    '#title' => t('Decrement stock of a product from a specific store and adjusts total stock.'),
    '#callback' => 'ica_up_nonproduct_items_decrement_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
    ),
    '#category' => t('Stock'),
  );
  $actions['ica_up_nonproduct_items_increment_stock'] = array(
    '#title' => t('Increment stock of a product from a specific store and adjusts total stock.'),
    '#callback' => 'ica_up_nonproduct_items_increment_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
    ),
    '#category' => t('Stock'),
  );
  

  return $actions;
}

function ica_up_nonproduct_items_decrement_stock($order, $product, $settings) {
  $store = $order->store_id;
  global $user;
  $stock_warnings = array();
  
  if (($stock = up_multi_stock($product->model, $store)) !== FALSE) {
    $threshold = db_result(db_query("SELECT threshold FROM {uc_product_stock} WHERE sku = '%s'", $product->model));
    if ((($stock - $product->qty) <= $threshold) && !in_array($product->model, array_keys($stock_warnings))) {
      $stock_warnings[$product->model] = $stock;
    }

    $stock -= $product->qty;
    up_multi_stock_adjust($product->model, -$product->qty, $store);
    uc_order_comment_save($order->order_id, $user->uid, t('The stock level for %model_name at location %store has been decreased to !qty.', array('%model_name' => $product->title, '%store' => $store, '!qty' => ($stock - $product->qty))));
    ica_up_nonproduct_items_action_adjust_total_stock($product);
  }

  if (!empty($stock_warnings) && variable_get('uc_stock_threshold_notification', FALSE)) {
    foreach ($stock_warnings as $model => $stock) {
      _uc_stock_send_mail($order, $stock);
    }
  }
}

function ica_up_nonproduct_items_increment_stock($order, $product, $settings) {      
  $store = $order->store_id;
  global $user;
      
  if (($stock = up_multi_stock($product->model, $store)) !== FALSE) {
    up_multi_stock_adjust($product->model, +$product->qty, $store);
    uc_order_comment_save($order->order_id, $user->uid, t('The stock level for %model_name at location %store has been increased to !qty.', array('%model_name' => $product->title, '%store' => $store, '!qty' => ($stock - $product->qty))));
    ica_up_nonproduct_items_action_adjust_total_stock($product);
  }
}

//Action for mutating stock
function ica_up_nonproduct_items_action_mutate_stock($order, $product, $amount, $settings) {
  $store = $order->store_id;
  global $user;
  $stock_warnings = array();
  //check if the stocklevel for the product at current location can be found 
  if (($stock = up_multi_stock($product->model, $store)) !== FALSE) {
    //Calculate the difference between newly inputted amount and the quantity that was already specified for the product
    $difference = $product->qty - $amount->amount ;
    //if there is a difference, update the current stock
    if ($difference != 0)
    { 
        //Set a warning if there are less products left in stock than specified in the threshold
        $threshold = db_result(db_query("SELECT threshold FROM {uc_product_stock} WHERE sku = '%s'", $product->model));
        if ((($stock + $difference) <= $threshold) && !in_array($product->model, array_keys($stock_warnings))) {
          $stock_warnings[$product->model] = $stock;
        }
        
        up_multi_stock_adjust($product->model, +$difference, $store);
        uc_order_comment_save($order->order_id, $user->uid, t('The stock level for %model_name at location %store has been decreased to !qty.', array('%model_name' => $product->title, '%store' => $store, '!qty' => ($stock + $difference))));
        ica_up_nonproduct_items_action_adjust_total_stock($product);
        
        if (!empty($stock_warnings) && variable_get('uc_stock_threshold_notification', FALSE)) {
            foreach ($stock_warnings as $model => $stock) {
                _uc_stock_send_mail($order, $stock);
            }
        }    
    }
  }
}

function ica_up_nonproduct_items_action_increment_refunded_stock($order, $product, $settings) {      
  $store = $order->store_id;
  global $user;
      
  if (($stock = up_multi_stock($product->model, $store)) !== FALSE) {
    up_multi_stock_adjust($product->model, +$product->qty*2, $store);
    uc_order_comment_save($order->order_id, $user->uid, t('The stock level for %model_name at location %store has been increased to !qty.', array('%model_name' => $product->title, '%store' => $store, '!qty' => ($stock - $product->qty*2))));
    ica_up_nonproduct_items_action_adjust_total_stock($product); 
  }
}

function ica_up_nonproduct_items_action_adjust_total_stock($product)
{
    if ($product != NULL)
    {
        $result = db_query('SELECT stock_level FROM {up_store_stock} WHERE model = "%d"', $product->model);
        $totalStock = 0;
        while($row = db_fetch_object($result))
        {
            $totalStock += $row->stock_level; 
        }
        uc_stock_set($product->model, $totalStock);   
    }
}
